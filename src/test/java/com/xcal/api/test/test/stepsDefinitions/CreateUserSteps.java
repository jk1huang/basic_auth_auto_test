package com.xcal.api.test.test.stepsDefinitions;

import com.xcal.api.test.pages.CreateUserPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static junit.framework.TestCase.assertTrue;

public class CreateUserSteps {

    private static CreateUserPage createUserPage = new CreateUserPage();

    @Given("^open create user page$")
    public void open_create_user_page() {
        createUserPage.openCreateUserPage();
    }

    @When("^click save button$")
    public void click_save_button() {
        createUserPage.submit();
    }

    @Then("^check error message count 5$")
    public void check_error_message_count_5() {
        assertTrue (createUserPage.checkErrorMessageCount(5));
    }

    @When("^fill user name and submit$")
    public void fill_user_name_and_submit() {
        createUserPage.fillUsername();
        createUserPage.submit();
    }

    @Then("^check error message count 4$")
    public void check_error_message_count_4() {
        assertTrue (createUserPage.checkErrorMessageCount(4));
    }

    @When("^fill display name and submit$")
    public void fill_display_name_and_submit() {
        createUserPage.fillDisplayName();
        createUserPage.submit();
    }

    @Then("^check error message count 3$")
    public void check_error_message_count_3() {
        assertTrue (createUserPage.checkErrorMessageCount(3));
    }


    @When("^fill email incorrect and submit$")
    public void fill_email_incorrect_and_submit() {
        createUserPage.fillEmailIncorrect();
        createUserPage.submit();
    }

    @When("^fill email correct and submit$")
    public void fill_email_correct_and_submit() {
        createUserPage.fillEmailCorrect();
        createUserPage.submit();
    }

    @Then("^check error message count 2$")
    public void check_error_message_count_2() {
        assertTrue (createUserPage.checkErrorMessageCount(2));
    }

    @When("^fill password less than 6 and submit$")
    public void fill_password_less_than_6_and_submit() {
        createUserPage.fillPasswordLessThan6();
        createUserPage.submit();
    }

    @When("^fill password correct and submit$")
    public void fill_password_correct_and_submit() {
        createUserPage.fillPasswordCorrect();
        createUserPage.submit();
    }

    @Then("^check error message count 1$")
    public void check_error_message_count_1() {
        assertTrue (createUserPage.checkErrorMessageCount(1));
    }

    @When("^fill different password and submit$")
    public void fill_different_password_and_submit() {
        createUserPage.fillDifferentPassword();
        createUserPage.submit();
    }

    @When("^fill same password and submit$")
    public void fill_same_password_and_submit() {
        createUserPage.fillSamePassword();
        createUserPage.submit();
    }

    @Then("^display success message$")
    public void display_success_message() {
        assertTrue (createUserPage.isSweetAlertMessageDisplay());
    }

}
