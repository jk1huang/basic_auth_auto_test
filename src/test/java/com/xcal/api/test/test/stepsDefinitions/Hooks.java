package com.xcal.api.test.test.stepsDefinitions;

import com.xcal.api.test.enums.Browsers;
import com.xcal.api.test.utils.AllureHelper;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Slf4j
public class Hooks {

    @After
    public void afterEachScenario(Scenario scenario) {
        AllureHelper.saveScreenshootOfScenario(scenario);
        //Browsers.quitDriver();
    }

    @AfterClass
    public static void tearDown() {
        Browsers.quitDriver();
    }

}