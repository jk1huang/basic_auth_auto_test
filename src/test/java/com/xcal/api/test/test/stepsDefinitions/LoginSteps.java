package com.xcal.api.test.test.stepsDefinitions;

import com.xcal.api.test.pages.CreateUserPage;
import com.xcal.api.test.pages.LoginPage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import lombok.extern.slf4j.Slf4j;

import static junit.framework.TestCase.assertTrue;

@Slf4j
public class LoginSteps {

	private static LoginPage loginPage;

	@Before
	public static void setup() {
		loginPage = new LoginPage();
	}

	@Given("^open login page$")
	public void open_login_page() {
		loginPage.openLogin();
	}

	@When("^input correct user name and incorrect password$")
	public void input_correct_user_name_and_incorrect_password() {
		loginPage.inputCorrectUsernameIncorrectPassword();
	}

	@Then("^will see error message with user name or password incorrect$")
	public void will_see_error_message() {
		assertTrue (loginPage.isSweetAlertMessageDisplay());
	}

	@When("^input user name that not exist$")
	public void input_incorrect_user_name_not_exist() {
		loginPage.inputCorrectUsernameIncorrectPassword();
	}

	@Then("^will see error message with user name not exist$")
	public void will_see_error_message_user_name_not_exist() {
		assertTrue (loginPage.isSweetAlertMessageDisplay());
	}

	@When("^clear user name and password$")
	public void clear_user_name_and_password() {
		loginPage.clearUsernamePassword();
	}

	@Then("^will see error message under user name and password$")
	public void will_see_error_message_under_user_name_and_password() {
		assertTrue (loginPage.isErrorMessageUnderUsernamePassword());
	}

	@When("^input correct user name and password$")
	public void input_correct_user_name_and_password() {
		loginPage.inputCorrectUsernamePassword();
	}

	@Then("^will go to home page$")
	public void will_go_to_home_page() {
		assertTrue (loginPage.isHomeUrl());
	}


}