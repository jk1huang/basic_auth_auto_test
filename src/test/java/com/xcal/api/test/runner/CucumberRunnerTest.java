package com.xcal.api.test.runner;

import com.xcal.api.test.enums.Browsers;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/resources/features",
plugin= {"io.qameta.allure.cucumberjvm.AllureCucumberJvm"},
glue = "com/xcal/api/test/test/stepsDefinitions",
tags= {"~@notImplemented"})

public class CucumberRunnerTest {

	@BeforeClass
	public static void tearUp() {
		Browsers.setWebDriver();
	}
	@AfterClass
	public static void tearDown() {
		Browsers.quitDriver();
	}
}