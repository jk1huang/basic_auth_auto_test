package com.xcal.api.test.pages;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.xcal.api.test.utils.Page;
import com.xcal.api.test.utils.HandleProperties;

@Slf4j
public class LoginPage extends Page {

	private static final By USER_NAME = By.id("username");
	private static final By PASSWORD = By.id("password");
	private static final By ERROR = By.className("error");
	private static final By SIDEBAR = By.className("sidebar");

	public void openLogin() {
		openUrl(this.getUiUrl());
	}

	public void inputCorrectUsernameIncorrectPassword() {
		fillInput("admin", USER_NAME);
		//fillInput("admin88ui7t", PASSWORD);
		//pressEnter(PASSWORD);
	}

	public void inputUsernameNoExist() {
		fillInput("adminZZZkkz", USER_NAME);
		fillInput("admin88ui7t", PASSWORD);
		pressEnter(PASSWORD);
	}

	public void clearUsernamePassword() {
		fillInput("", USER_NAME);
		fillInput("", PASSWORD);
		pressEnter(PASSWORD);
	}

	public void inputCorrectUsernamePassword() {
		fillInput("admin", USER_NAME);
		fillInput("admin", PASSWORD);
		pressEnter(PASSWORD);
	}

	public boolean isErrorMessageUnderUsernamePassword() {
		List<WebElement> ws = getElements(ERROR);
		return (ws != null && ws.size() == 2) ? true : false;
	}

	public boolean isHomeUrl() {
		WebElement sidebar = waitElement(SIDEBAR);
		return sidebar != null;
	}

}