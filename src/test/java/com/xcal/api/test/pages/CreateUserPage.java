package com.xcal.api.test.pages;

import com.xcal.api.test.utils.Page;
import org.openqa.selenium.By;
import org.apache.commons.lang3.RandomStringUtils;

public class CreateUserPage extends Page {

    private static final By LOGIN = By.id("login");
    private static final By NAME = By.id("name");
    private static final By EMAIL = By.id("email");
    private static final By NEW_PASSWORD = By.id("newpassword");
    private static final By CONFIRM_PASSWORD = By.id("confirmpassword");

    private String password = "123iopa";
    private String username = "test_" + RandomStringUtils.randomAlphabetic(6);

    public void openCreateUserPage() {
        String createUrl = getUiUrl() + "/create-user";
        openUrl(createUrl);
    }

    public void submit() {
        pressEnter(LOGIN);
    }

    public void fillUsername() {
        fillInput(username, LOGIN);
    }

    public void fillDisplayName() {
        fillInput(username, NAME);
    }

    public void fillEmailIncorrect() {
        fillInput("usertest1", EMAIL);
    }

    public void fillEmailCorrect() {
        fillInput(username + "@google.com", EMAIL);
    }

    public void fillPasswordLessThan6() {
        fillInput("112", NEW_PASSWORD);
    }

    public void fillPasswordCorrect() {
        fillInput(password, NEW_PASSWORD);
    }

    public void fillDifferentPassword() {
        fillInput("999ccsleks", CONFIRM_PASSWORD);
    }

    public void fillSamePassword() {
        fillInput(password, CONFIRM_PASSWORD);
    }

}
