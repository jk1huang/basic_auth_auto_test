@login
Feature: Test login page
  This will test login page
  Include fail case and success case

  Scenario: password incorrect
    Given open login page
    When input correct user name and incorrect password
#    Then will see error message with user name or password incorrect

#  Scenario: User name not exist
#    Given open login page
#    When input user name that not exist
#    Then will see error message with user name not exist
#
#  Scenario: User name and password are empty
#    Given open login page
#    When clear user name and password
#    Then will see error message under user name and password
#
#  Scenario: User name/password correct
#    Given open login page
#    When input correct user name and password
#    Then will go to home page
#
## Test Create User #
#  Scenario: create user with all empty info
#    Given open create user page
#    When click save button
#    Then check error message count 5
#
#  Scenario: create user with user name not empty
#    When fill user name and submit
#    Then check error message count 4
#
#  Scenario: create user with display name not empty
#    When fill display name and submit
#    Then check error message count 3
#
#  Scenario: create user with email incorrect
#    When fill email incorrect and submit
#    Then check error message count 3
#
#  Scenario: create user with email correct
#    When fill email correct and submit
#    Then check error message count 2
#
#  Scenario: create user with password less than 6
#    When fill password less than 6 and submit
#    Then check error message count 2
#
#  Scenario: create user with password correct
#    When fill password correct and submit
#    Then check error message count 1
#
#  Scenario: create user with different password
#    When fill different password and submit
#    Then check error message count 1
#
#  Scenario: create user with same password
#    When fill same password and submit
#    Then display success message
#
## Test create project #
#  Scenario: create project with all empty info
#    Given open create user page
#    When click save button
#    Then check error message count 5